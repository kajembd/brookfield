-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: mysql51-029.wc2:3306
-- Generation Time: Mar 15, 2013 at 01:46 PM
-- Server version: 5.1.61
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `495229_borkfield`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_status`
--

CREATE TABLE IF NOT EXISTS `email_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `survey_id` bigint(20) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `mail_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hit`
--

CREATE TABLE IF NOT EXISTS `hit` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `website` bigint(20) NOT NULL,
  `linkedin` bigint(20) NOT NULL,
  `twitter` bigint(20) NOT NULL,
  `last_hit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hit`
--

INSERT INTO `hit` (`id`, `website`, `linkedin`, `twitter`, `last_hit`) VALUES
(1, 4, 4, 2, '2013-03-14 18:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `major` varchar(255) NOT NULL,
  `age` varchar(100) NOT NULL,
  `graduating_year` int(4) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `flexible_working_hour` tinyint(4) NOT NULL,
  `vacation` tinyint(4) NOT NULL,
  `benifits` tinyint(4) NOT NULL,
  `type_of_industry` tinyint(4) NOT NULL,
  `company_reputation` tinyint(4) NOT NULL,
  `company_culture` tinyint(4) NOT NULL,
  `base_salary` tinyint(4) NOT NULL,
  `location` tinyint(4) NOT NULL,
  `job_responsibility` tinyint(4) NOT NULL,
  `employer_choice` tinyint(4) NOT NULL,
  `other` varchar(255) NOT NULL,
  `email_me` tinyint(4) NOT NULL,
  `yes` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `block`, `name`, `signature`, `email`, `major`, `age`, `graduating_year`, `gender`, `flexible_working_hour`, `vacation`, `benifits`, `type_of_industry`, `company_reputation`, `company_culture`, `base_salary`, `location`, `job_responsibility`, `employer_choice`, `other`, `email_me`, `yes`, `user_id`, `modified`, `created`) VALUES
(1, '1', 'Gurpreetmally', 'Gurpreetmally', 'Gurpreet.gill@hotmail.com', 'Hr', '18-21', 2014, 'Female', 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, '', 0, 1, 1, '2013-03-13 18:23:50', '2013-03-13 08:23:50'),
(2, '1', 'Jeff Woods', 'Jeff Woods', 'jeff@woodscreative.ca', 'Fine Arts', '26-30', 2014, 'Male', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Test', 1, 1, 1, '2013-03-13 18:58:37', '2013-03-13 08:58:37'),
(4, '1', 'Jeff Woods', 'Jeff Woods', 'jeff@woodscreative.ca', 'Fine Arts', '26-30', 2014, 'Male', 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 19:23:47', '2013-03-13 09:23:47'),
(5, '1', 'Gurpreet', 'Gurpreet', 'Gurpreetmally@hotmail.com', 'Hr', '18-21', 2014, 'Female', 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, '', 0, 1, 1, '2013-03-13 20:04:51', '2013-03-13 10:04:51'),
(6, '1', 'Gurpreet', 'Gurpreet', 'Gurpreet@hotmail.com', 'Hr', '18-21', 2912, 'Female', 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, '', 1, 1, 1, '2013-03-13 20:18:39', '2013-03-13 10:18:39'),
(7, '1', 'Dan lama', 'Dan lama', 'Lama.dandaniel@yahoo.ca', 'It', '18-21', 2012, 'Male', 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, '', 0, 1, 1, '2013-03-13 20:39:56', '2013-03-13 10:39:56'),
(8, '1', 'Jessica', 'Jessica willis', 'Jessie.1122@hotmail.com', 'Open studies', '18-21', 2017, 'Female', 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, '', 0, 1, 1, '2013-03-13 20:46:36', '2013-03-13 10:46:36'),
(9, '1', 'Xuemei Lu. ', 'Xuemei lu', 'Xlu598@mtroyal.ca', 'Accounting', '41+', 2015, 'Female', 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-13 20:47:34', '2013-03-13 10:47:34'),
(10, '2', 'Elizabeth Morgan', 'Elizabeth Morgan', 'Lizzie_bob11@hotmail.com', 'Psychology', '18-21', 2016, 'Female', 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, '', 1, 1, 1, '2013-03-13 20:54:06', '2013-03-13 10:54:06'),
(11, '2', 'Chloe Mansfield ', 'Chloe', 'Cmans738@mtroyal.ca', 'Public relations', '18-21', 2016, 'Female', 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, '', 1, 1, 1, '2013-03-13 20:55:13', '2013-03-13 10:55:13'),
(12, '2', 'Karolina Staszczak', 'Karolina Staszczak', 'K.staszczak@hotmail.com', 'General management', '18-21', 2013, 'Female', 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 21:00:15', '2013-03-13 11:00:15'),
(13, '1', 'Jeff Woods', 'Jeff Woods', 'jeff@woodscreative.ca', 'Test', '26-30', 2014, 'Male', 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, '', 1, 1, 1, '2013-03-13 21:00:24', '2013-03-13 11:00:24'),
(14, '1', 'Dan Hiatt', 'Dan Hiatt', 'danielhiatt@shaw.ca', 'Sociology (Hons)', '22-25', 2013, 'Male', 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-13 21:02:00', '2013-03-13 11:02:00'),
(15, '1', 'Amber', 'Ap', 'Amberkbp@gmail.com', 'English', '22-25', 2014, 'Female', 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, '', 1, 1, 1, '2013-03-13 21:07:37', '2013-03-13 11:07:37'),
(16, '1', 'Awet Abraha', 'Awet', 'Awet.a@shaw.ca', 'Accounting', '18-21', 2015, 'Male', 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, '', 0, 1, 1, '2013-03-13 21:11:57', '2013-03-13 11:11:57'),
(17, '2', 'Sunayana Mendonca', 'Sunayana M', 'sunayanam@hotmail.ca', 'Psychology ', '26-30', 2013, 'Female', 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-13 21:13:37', '2013-03-13 11:13:37'),
(18, '2', 'Josh litzenberger', 'Josh litzenberger', 'Josh.litzenberger@gmail.com', 'Management marketing minor', '22-25', 2013, 'Male', 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 21:14:29', '2013-03-13 11:14:29'),
(19, '1', 'Danielle mah', 'Danielle Mah', 'Danimah@hotmail.com', 'Accounting', '41+', 2000, 'Female', 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, '', 0, 1, 1, '2013-03-13 21:19:07', '2013-03-13 11:19:07'),
(20, '2', 'Logan Krupa', 'Logan Krupa', 'logankrupa@hotmail.ca', 'Information Design', '22-25', 2016, 'Male', 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, '', 0, 1, 1, '2013-03-13 21:21:28', '2013-03-13 11:21:28'),
(21, '2', 'Luisangelica Manzano', 'Lm', 'luisangelicam@gmail.com', 'GM/Marketing', '18-21', 2015, 'Female', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 21:26:37', '2013-03-13 11:26:37'),
(22, '1', 'Saber khan', 'Saner khan', 'Skhan710@mtroyal.ca', 'Management', '22-25', 2013, 'Female', 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-13 21:30:37', '2013-03-13 11:30:37'),
(23, '2', 'Ellen Phillips', 'Ep', 'Ephil634@mtroyal.com', 'Policy studies', '18-21', 2015, 'Female', 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, '', 0, 1, 1, '2013-03-13 21:34:11', '2013-03-13 11:34:11'),
(24, '1', 'Kevin Merkl', 'Kevin Merkl', 'blkw49@hotmail.com', 'Edication', '18-21', 2015, 'Male', 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-13 21:36:19', '2013-03-13 11:36:19'),
(25, '1', 'Francesca sutanto', 'Fs', 'Francesca.sutanto@hotmail.com', 'Public relations', '22-25', 2014, 'Female', 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, '', 0, 1, 1, '2013-03-13 21:38:05', '2013-03-13 11:38:05'),
(26, '1', 'Wang', 'Wang', 'Wangxu0623@hotmail.com', 'Engineering', '26-30', 2017, 'Male', 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, '', 0, 1, 1, '2013-03-13 21:40:30', '2013-03-13 11:40:30'),
(27, '1', 'Aom', 'Aom', 'Dana_aom@hotmail.com', 'Bba', '18-21', 2013, 'Female', 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, '', 0, 1, 1, '2013-03-13 21:45:17', '2013-03-13 11:45:17'),
(28, '1', 'Aly macDonald', 'Aly MacDonald', 'Amacd695@mtroyal.ca', 'Psychology', '22-25', 2014, 'Female', 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, '', 1, 1, 1, '2013-03-13 21:47:22', '2013-03-13 11:47:22'),
(29, '1', 'Yaryna maik', 'Yarynmaik', 'YarynaMaik.491@hotmail.com', 'Psych', '22-25', 2012, 'Female', 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, '', 1, 1, 1, '2013-03-13 21:50:16', '2013-03-13 11:50:16'),
(30, '2', 'Kathryn Pettitt', 'Kathryn pettitt', 'Kpettitt009@gmail.com', 'Political science', '26-30', 2013, 'Female', 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, '', 0, 1, 1, '2013-03-13 21:52:13', '2013-03-13 11:52:13'),
(31, '2', 'Lisa virzi', 'Lisa virzi', 'Lisa_virzi25@yahoo.ca', 'Human resources', '26-30', 2014, 'Male', 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-13 21:53:02', '2013-03-13 11:53:02'),
(32, '1', 'Clayton Goodwin ', 'Cgoodwin', 'cgoodwin5707@gmail.com', 'Communications', '22-25', 2013, 'Male', 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, '', 1, 1, 1, '2013-03-13 21:56:58', '2013-03-13 11:56:58'),
(33, '2', 'Eric persson', 'Eric persson', 'ibic134@gmail.com', 'Accounting', '22-25', 2013, 'Male', 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, '', 0, 1, 1, '2013-03-13 22:04:30', '2013-03-13 12:04:30'),
(34, '2', 'Chris Gonzalez', 'Chris g', 'cgonz454@mtroyal.ca', 'Computer science', '18-21', 2016, 'Male', 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, '', 1, 1, 1, '2013-03-13 22:05:17', '2013-03-13 12:05:17'),
(35, '1', 'Taryn', 'Tc', 'Taryn.elyse@gmail.com', 'Bba', '22-25', 2013, 'Female', 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-13 22:07:53', '2013-03-13 12:07:53'),
(36, '1', 'Paul hunka', 'Paul hunka', 'Paul.hunka@gmail.com', 'Accounting', '22-25', 2014, 'Male', 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-13 22:08:49', '2013-03-13 12:08:49'),
(37, '2', 'Spencer batchelor', 'Spencer batchelor', 'Spencer.batchelor@hotmail.com', 'Psychology', '18-21', 2016, 'Female', 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, '', 1, 1, 1, '2013-03-13 22:12:01', '2013-03-13 12:12:01'),
(38, '2', 'Cameron Gavin', 'Cameron Gavin', 'Cameron.gavin@hotmail.com', 'Engineering', '18-21', 2016, 'Male', 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-13 22:17:14', '2013-03-13 12:17:14'),
(39, '1', 'Dan zarzar', 'Dan zarzar', 'Zzzdac@yahoo.com', 'Interior design', '18-21', 2016, 'Male', 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-13 22:22:05', '2013-03-13 12:22:05'),
(40, '2', 'Jolene follgard', 'Jolene', 'Jfollgard@gmail.com', '', '22-25', 2014, 'Female', 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, '', 1, 0, 1, '2013-03-13 22:27:49', '2013-03-13 12:27:49'),
(41, '2', 'Jolene follgard', 'Jolene', 'Jfollgard@gmail.com', '', '22-25', 2014, 'Female', 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, '', 1, 0, 1, '2013-03-13 22:27:49', '2013-03-13 12:27:49'),
(42, '2', 'Jolene follgard', 'Jolene', 'Jfollgard@gmail.com', '', '22-25', 2014, 'Female', 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, '', 1, 1, 1, '2013-03-13 22:27:50', '2013-03-13 12:27:50'),
(43, '1', 'Helen boric', 'Helen boric', 'Hozic@shaw.ca', 'Accouting', '41+', 2013, 'Female', 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, '', 0, 1, 1, '2013-03-13 22:31:20', '2013-03-13 12:31:20'),
(44, '1', 'Ifeoma adeagbo', 'Ifeoma', 'Ifeomaadeagbo@yahoo.com', 'Accounting', '41+', 2012, 'Female', 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-13 22:35:52', '2013-03-13 12:35:52'),
(45, '1', 'Madison', 'Madison', 'Fordmaddie1@hotmail.com', 'Sociology', '18-21', 2016, 'Male', 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, '', 0, 1, 1, '2013-03-13 23:17:53', '2013-03-13 13:17:53'),
(46, '2', 'Wiser Ali', 'Qaiser', 'Ali.qaiser@yahoo.com', 'Accounting', '26-30', 2014, 'Male', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 23:19:17', '2013-03-13 13:19:17'),
(47, '1', 'Jermyn Voon', 'Jermyn Voon', 'Jermyn.voon@gmail.com', 'Management,marketing & accounting ', '22-25', 2013, 'Male', 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, '', 1, 1, 1, '2013-03-13 23:22:38', '2013-03-13 13:22:38'),
(48, '1', 'Anna lee', 'Anna lee', 'Annaw.lee625@gmail.com', 'Policy studies', '22-25', 2014, 'Female', 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 23:35:11', '2013-03-13 13:35:11'),
(49, '1', 'Marekret', 'Marekret Markos', 'Mmark267@mtroyal.ca', 'Policy studies ', '18-21', 3, 'Female', 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, '', 0, 1, 1, '2013-03-13 23:35:24', '2013-03-13 13:35:24'),
(50, '1', 'Ray goff', 'Ray goff', 'Rgoff524@mtroyal.ca', 'Undeclared', '18-21', 2018, 'Male', 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-13 23:42:55', '2013-03-13 13:42:55'),
(51, '2', 'Deng', 'deng', 'dengawak3@gmail.com', 'Business Management ', '18-21', 2015, 'Male', 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, '', 1, 1, 1, '2013-03-13 23:43:21', '2013-03-13 13:43:21'),
(52, '1', 'Powys', 'Pouya', 'Pouyamosstajiri@gmail.com', 'Civil engineer ', '31-40', 2013, 'Male', 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, '', 0, 1, 1, '2013-03-13 23:52:59', '2013-03-13 13:52:59'),
(53, '', 'Rachael jones', 'Rachael Jones', 'Rjone702@mtroyal.ca', 'Science', '18-21', 2016, 'Female', 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-14 00:04:43', '2013-03-13 14:04:43'),
(54, '1', 'Kelsey Matlock', 'Kelsey Matlock', 'Kelseymatlock92@hotmail.com', 'English', '18-21', 2016, 'Female', 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-14 00:05:50', '2013-03-13 14:05:50'),
(55, '2', 'Katie temple', 'Katie temple', 'ktemp233@mtroyal.ca', 'Public relations', '18-21', 2014, 'Female', 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, '', 1, 1, 1, '2013-03-14 00:09:59', '2013-03-13 14:09:59'),
(56, '1', 'Linh', 'Linh', 'npham073@mtroyal.ca', 'Ef1', '18-21', 2013, 'Female', 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, '', 0, 0, 1, '2013-03-14 00:10:28', '2013-03-13 14:10:28'),
(57, '1', 'Linh', 'Linh', 'npham073@mtroyal.ca', 'Ef1', '18-21', 2013, 'Female', 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, '', 0, 0, 1, '2013-03-14 00:14:28', '2013-03-13 14:14:28'),
(58, '1', 'Linh', 'Linh', 'npham073@mtroyal.ca', 'Ef1', '18-21', 2013, 'Female', 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, '', 0, 1, 1, '2013-03-14 00:14:29', '2013-03-13 14:14:29'),
(59, '1', 'Ivan', 'Il', 'Ilaumc@gmail.com', 'Engineering', '22-25', 2010, 'Male', 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-14 00:17:38', '2013-03-13 14:17:38'),
(60, '1', 'Ayzer Cruz ', 'Ayzer Cruz', 'ayzer.cruz@gmail.com', 'Accounting', '22-25', 2015, 'Female', 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, '', 0, 1, 1, '2013-03-14 00:19:39', '2013-03-13 14:19:39'),
(61, '2', 'Taiaba', 'Taiaba', 'Utaiaba@hotmail.com', 'Sciences', '22-25', 2017, 'Male', 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-14 00:23:17', '2013-03-13 14:23:17'),
(62, '1', 'Jackson', 'Dave jackson', 'D.h.jackson@shaw.ca', 'History', '41+', 2019, 'Male', 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, '', 1, 1, 1, '2013-03-14 00:31:36', '2013-03-13 14:31:36'),
(63, '2', 'Ola', 'Ola Adeniyi', 'Ola.aden@gmail.com', 'Human Resources', '22-25', 2013, 'Female', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-14 00:43:08', '2013-03-13 14:43:08'),
(64, '1', 'Sarah Aves', 'Sarah aves', 'sarahaves.marketing@gmail.com', 'Marketing', '26-30', 2013, 'Female', 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, '', 1, 1, 1, '2013-03-14 00:46:12', '2013-03-13 14:46:12'),
(65, '1', 'Jerome', 'Jerome Makasiar', 'Jmaka526@mymru.ca', 'Education-elementary', '22-25', 2015, 'Male', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, '', 0, 1, 1, '2013-03-14 00:49:08', '2013-03-13 14:49:08'),
(66, '2', 'Sarah kemp', 'Sarah kemp', 'Sehkemp@telus.net', 'Child care', '18-21', 2014, 'Female', 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, '', 1, 1, 1, '2013-03-14 00:57:19', '2013-03-13 14:57:19'),
(67, '2', 'Mitchell Gonzalez', 'Mitchell Gonzalez', 'mitchell.gonzalez@gmail.com', 'Engineering', '18-21', 2015, 'Male', 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, '', 1, 1, 1, '2013-03-14 01:00:13', '2013-03-13 15:00:13'),
(68, '2', 'Johnson Ngini', 'Chie', 'Johnsonngini@gmail.com', 'Accounting', '18-21', 2014, 'Male', 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, '', 0, 1, 1, '2013-03-14 01:08:34', '2013-03-13 15:08:34'),
(69, '1', 'Jeff ', 'Jeff', 'Jeffystyle@hotmail.com', 'Marketing', '26-30', 20154, 'Male', 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, '', 0, 1, 1, '2013-03-14 01:14:30', '2013-03-13 15:14:30'),
(70, '1', 'Zachary Ziobron', 'Zachary ziobron', 'ziobronz@gmail.com', 'Accounting', '18-21', 2015, 'Male', 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, '', 1, 0, 1, '2013-03-14 01:23:34', '2013-03-13 15:23:34'),
(71, '1', 'Jeff Woods', 'Jeff Woods', 'jeff@woodscreative.ca', 'Test', '18-21', 2014, 'Male', 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, '', 1, 1, 1, '2013-03-14 04:08:03', '2013-03-13 18:08:03'),
(72, '1', 'Steve Kenway', 'Steve Kenway', 'steven.kenway@gmail.com', 'Computer Science', '31-40', 2004, 'Male', 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 'Cool Projects', 1, 0, 1, '2013-03-14 04:51:47', '2013-03-13 18:51:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `modified`, `created`) VALUES
(1, 'Admin', 'Admin', 'admin', 'admin', '2013-03-07 12:35:37', '2013-03-19 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
