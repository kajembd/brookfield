<div style="width: 800px; font-family: 'Arial'; background: url('http://instaonline.com/websites/brookfield/images/email-header-bg.png') no-repeat; padding-top: 251px; padding-bottom: 50px; margin: 0 auto;" >
    <div style=" width: 660px; margin: 0 auto;">
        <div style="color: #00467f; font-size: 24px; font-weight: bold;">WE HAVE A WINNER</div>
        <div style="text-align: justify; font-size: 14px;">
            Drum roll Please....we have chosen a winner for the ultimate student gift basket and wanted
			to thank you for trying out your luck with the Broofield Tumbler at the MRU Carrer Fair.
        </div>
		<div style="color: #00467f; font-size: 24px; padding-top: 30px; font-weight: bold;">WE'RE HIRING</div>
		<div style="text-align: justify; font-size: 14px;">
            Brookfield is currently hiring for a variety of summer positions! Checkout this exciting 
			opportunities at 
        </div>
		<table>
            <tr>
                <td style="padding-top: 15px;">
					<a style="margin-top: -10px;"target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=website">
						<img style="color: #004580;" src="http://www.instaonline.com/websites/brookfield/images/get-a-job.png" title="Get a job" />
					</a>
				</td>
				<td style="font-weight: bold; padding-top: 23px; font-size: 16px; color: #555455;">
					THE BROOKFIELD CAREERS PAGE<br/>
					<a style="color: #004580;" target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=website">brookfieldrp.com/careers</a>
				</td>
			</tr>
			<tr>
                <td style="padding-top: 10px;">
					<a style="color: #004580;" target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=linkedin">
						<img style="margin-top: -10px;" src="http://www.instaonline.com/websites/brookfield/images/in-icon.png" title="Twitter">
					</a>
				</td>
				<td style="font-weight: bold; padding-top: 10px; font-size: 16px; color: #555455;">
					FOLLOW OUR NETWORK IN LINKDIN<br/>
					<a style="color: #004580; " target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=linkedin"> linkedin.com/company/brookfield-residential </a>
				</td>
			</tr>
			<tr>
                <td style="padding-top: 10px;">
					<a target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=twitter">
						<img style="margin-top: -10px;" src="http://www.instaonline.com/websites/brookfield/images/twitter-icon.png" title="Twitter"></a>
				</td>
				<td style="font-weight: bold; padding-top: 15px; font-size: 16px; color: #555455;">
					FOLLOW US ON TWITTER<br/>
					<a style="color: #004580; " target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=twitter"> twitter.com/buildbrookfield </a>
				</td>
			</tr>
        </table>
        <div style="color: #00467f; font-size: 24px; font-weight: bold; padding-top: 30px;"> About Us </div>
        <div style="color: #808080; font-size: 14px; text-align: justify; font-family: 'Roman' ">
            <p>A leading North American land developer, home builder and real estate innovator, Brookfield Residential Properties (BRP) operates in ten 
            major markets.  BRP entitles and develops land to create master-planned communities and builds and sells lots to third-party builders 
            as well as to Brookfield Homes.  Brookfield Homes designs, builds and markets single and multi-family residences in select markets and 
            communities. BRP also participates in selected, strategic real estate opportunities, including infill projects, mixed-use developments, 
            infrastructure projects, and joint ventures.</p> 

            <p>As one of Canada's Top 50 employers, you will be a part of an inspiring environment where people strive to excel and provide an exceptional
            experience both internally and externally.  We are always looking for enthusiastic and passionate people to join our team.  Headquartered
            in SW Calgary, our values of "Passion, Integrity and Community" defines both who we are as a company and why you'd want to work at 
            Brookfield Residential.  We employ some of the most skilled and versatile people in our industry, who consistently deliver solutions
            for our clients, both external and internal. </p>  
        </div>        
        <div><img style="width: 200px; height: 71px;" src="http://www.instaonline.com/websites/brookfield/images/ui-icons_222222_256x240.png" title="Get a job"></div>
    </div>
</div>
