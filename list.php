<?php
include 'include/header.php';
include 'include/db.php';   



$sql = "SELECT survey.*, email_status.checked as email_view FROM survey survey LEFT JOIN email_status email_status on email_status.survey_id = survey.id";

$result = mysql_query($sql); 

?>
<div class="blue-bar">
    <div class="brook-field">
        <div class="wrapper">
            <div class="label-1-text">Brookfield</div>
            <div class="label-2-text">Residential</div>
        </div>
    </div>
</div>
<style>
    .detail-tbl{
        
    }
    .detail-tbl th, .detail-tbl td{
        padding: 5px; 
        text-align: left;
        border-bottom: 1px solid #eaeaea;
    }
    .detail-tbl th{
        background: #f0b310;   
        color: #ffffff;
    }

</style>


<table style="margin-bottom: 70px;" width="100%" cellpadding="0" cellspacing="0" class="detail-tbl">
    <tr class="yellow-bar">
        <th>Name</th>
        <th>Signature</th>
        <th>Email</th>
        <th>Major</th>
        <th>Age</th>
        <th>Graduating <br /> Year</th>
        <th>Gender</th>
        <th style="text-align: center">Three Things</th>
        <th>Other</th>
        <th>Manage Block</th>
        <th>Email Checked</th>
    </tr>
<?php
    while($row = mysql_fetch_array($result)){
?>
    <tr>
        <td><?php echo $row['name']; ?></td>
         <td><?php echo $row['signature']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><?php echo $row['major']; ?></td>
        <td><?php echo $row['age']; ?></td>
        <td><?php echo $row['graduating_year']; ?></td>
        <td><?php echo $row['gender']; ?></td>
        <td>
            <?php
                echo $row['flexible_working_hour'] == 1 ? "Flexible Working Hour, " : "";
                echo $row['vacation'] == 1 ? "Vacation, " : "";
                echo $row['benifits'] == 1 ? "Benifits, " : "";
                echo $row['type_of_industry'] == 1 ? "Type of Industry, " : "";
                echo $row['company_reputation'] == 1 ? "Company Reputation, " : "";
                echo $row['company_culture'] == 1 ? "Company Culture, " : "";
                echo $row['base_salary'] == 1 ? "Base Salary, " : "";
                echo $row['location'] == 1 ? "Location, " : "";
                echo $row['job_responsibility'] == 1 ? "Job Responsibility, " : "";
                echo $row['employer_choice'] == 1 ? "Employer Choice, " : "";
            ?>
        </td>
        <td><?php echo $row['gender']; ?></td>
        <?php 
            if($row['yes'] == '1'){
                $yes = "Yes";
            }else{
                $yes = "No";
            }
        ?>
        <td><?php echo $yes; ?></td>
        <td><?php echo $row['email_view'] == 1 ? 'Yes' : 'No'; ?></td>
    </tr>
<?php
    }
?>
</table>
<?php
    include 'include/footer.php';
?>

