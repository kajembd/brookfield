<div style="width: 800px; font-family: 'Arial'; background: url('http://instaonline.com/websites/brookfield/images/email-header-bg.png') no-repeat; padding-top: 251px; padding-bottom: 50px; margin: 0 auto;" >
    <div style=" width: 660px; margin: 0 auto;">
        <div style="color: #00467f; font-size: 24px; font-weight: bold;">Thank you for visting our booth!</div>
        <div style=" color: #00467f; font-size: 14px;">
            If your name was entered to win the ultimate student gift basket, we will be drawing the name soon and will be contacting the winner by email.
        </div>
        <div style="color: #00467f; font-size: 24px; font-weight: bold; padding-top: 30px;"> About Us </div>
        <div style="color: #808080; font-size: 14px; text-align: justify; font-family: 'Roman' ">
            <p>A leading North American land developer, home builder and real estate innovator, Brookfield Residential Properties (BRP) operates in ten 
            major markets.  BRP entitles and develops land to create master-planned communities and builds and sells lots to third-party builders 
            as well as to Brookfield Homes.  Brookfield Homes designs, builds and markets single and multi-family residences in select markets and 
            communities. BRP also participates in selected, strategic real estate opportunities, including infill projects, mixed-use developments, 
            infrastructure projects, and joint ventures.</p> 

            <p>As one of Canada's Top 50 employers, you will be a part of an inspiring environment where people strive to excel and provide an exceptional
            experience both internally and externally.  We are always looking for enthusiastic and passionate people to join our team.  Headquartered
            in SW Calgary, our values of "Passion, Integrity and Community" defines both who we are as a company and why you'd want to work at 
            Brookfield Residential.  We employ some of the most skilled and versatile people in our industry, who consistently deliver solutions
            for our clients, both external and internal. </p>  
        </div>
        <div style="color: #00467f; font-size: 24px; padding-bottom: 5px; font-weight: bold;">Interested in learning more about us?</div>
        <table>
            <tr>
                <td><a target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=website"><img src="http://www.instaonline.com/websites/brookfield/images/get-a-job.png" title="Get a job" /></a></td>
                <td><a target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=linkedin"><img src="http://www.instaonline.com/websites/brookfield/images/in-icon.png" title="Twitter"></a></td>
                <td><a target="_blank" href="http://www.instaonline.com/websites/brookfield/hit.php?type=twitter"><img src="http://www.instaonline.com/websites/brookfield/images/twitter-icon.png" title="Twitter"></a></td>
            </tr>
        </table>
        <div><img src="http://www.instaonline.com/websites/brookfield/images/best-small-medium-employer.png" title="Get a job"></div>
    </div>
</div>
