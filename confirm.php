<?php
    include 'include/header.php';
    $block = isset($_GET['block'])? $_GET['block'] : '1'
?>

<div class="wrapper-confirm"> 
    <div class="confirm-content">This participant has selected ( <?php echo $block; ?> ) blocks. <br /> Did the attendee successfully manage to stack all of the blocks? </div>
    <div class="yes-no-button">
        <div class="no" onclick="home();">No</div>
        <div class="yes" onclick="yes();">Yes</div>
        <div class="clear"></div>
    </div>
</div> 

<div id="submit-confirm-popup" style="display: none;">
    <div class="submit-confirm-popup">
        <div class="thank-you">Survey Complete! </div>
        <div class="return">Click continue to return to the <br /> home screen.</div>
        <div class="back-to-home" onclick="home();">Continue</div>
    </div>
</div>
 
<?php
    include 'include/footer.php';
?>

<script type="text/javascript">
    function yes(){
         $.ajax({                    
             url: 'update.php',          
             success: function(){
                 $.fancybox({
                    content: $('#submit-confirm-popup').html()
                 });
             }
         });        
    }
    
    function home(){
        window.location = "index.php";
    }
    
    $(document).ready(function(){
       var div_height = $('.wrapper-confirm').height(); 
       var window_height = $(window).height();
       var padding_top = (window_height/2) - div_height;
       
       $('.wrapper-confirm').css('padding-top', padding_top);
       
    });
</script>