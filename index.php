<?php include 'include/header.php'; ?>

<div class="blue-bar">
    <div class="brook-field">
        <div class="wrapper">
            <div class="label-1-text">Brookfield</div>
            <div class="label-2-text">Residential</div>
        </div>
    </div>
</div>
<div class="yellow-bar">
    <div class="wrapper">
        <div class="label-1-yellow-text">Brookfield</div>
        <div class="label-2-text">Home</div>
    </div>
</div>

<div class="home-wrapper">
    <?php if( empty( $_SESSION['brokerfield']['user_id'] ) ) { ?>
    <div class="login">
        <form name="login" id="login" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >           
            <div id="login-error" class="error" colspan="2">
                 <?php if( !empty( $_POST ) ) { echo "Invalid username or password!"; } ?>
            </div>                   
                
            <table>                              
                <tr>
                    <td>Username: </td>
                    <td> <input type="text" name="username"> </td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td> <input type="password" name="password"> </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> <button style="width: 100px; height: 30px; border: 1px solid #cdcdce; font-weight: bold;" class="btn btn-primary" onclick="proceedToSurvey();" > Submit </button></td>
                </tr>
            </table>
        </form>
    </div>
    <?php } else { ?>
    <div class="brookfield-tumble-tower-madness"></div>
    <div class="guess">Guess the number of blocks you think you can pull from the bottom and stack back on top (maximum of 2).</div>
    <div class="if-you-manage">If you manage to restack the number of blocks you wagered , we will enter that amount of ballots in your name for a draw to win the ultimate student gift basket, knock it over and all of your ballets are removed from our draw.</div>
    <div class="how-many">How many blocks do you think <br /> you can pull and restack?</div>
    
	<form method="post" id="form_block" action="survey.php" >
		<input type="hidden" name="block" id="block" value="1" />
		<table align="center" class="block-buttons">
			<tr>
				<td align="center">
					<label class="radio-btn radio-on" data-value="1" id="blockOne"></label>
				</td>
				<td align="center">
					<label class="radio-btn radio-off" data-value="2" id="blockTwo"></label>
				</td>
			</tr>
			<tr>
				<td align="center">
					<lable for="blockOne">One Block</lable>
				</td>
				<td align="center">
					<label for="blockTwo">Two Blocks</label>
				</td>
			</tr>
			<tr>
				<td style="height: 100px;" colspan="2"> <button type="submit" style="width: 150px; border: 1px solid #cdcdce; font-weight: bold;" class="btn btn-large btn-primary"> Next </button></td>
			</tr>
		</table>
	</form>
    
    <?php } ?>
</div>

<script type="text/javascript">
    
    $(function(){
         $(".radio-btn").on("click", function(){
             var value = $(this).attr("data-value"); 
             if(value == 1){
                 $("#blockOne").removeClass("radio-off").addClass("radio-on"); 
                 $("#blockTwo").removeClass("radio-on").addClass("radio-off"); 
             }else{
                 $("#blockTwo").removeClass("radio-off").addClass("radio-on"); 
                 $("#blockOne").removeClass("radio-on").addClass("radio-off"); 
             }
             $("#block").val(value); 
         });
    });
</script>

<?php
    include 'include/footer.php';
?>
