<?php
include 'include/db.php';
$page = explode( '/', $_SERVER['PHP_SELF'] );
session_start();
if( $page[count($page) - 1] != 'index.php' && empty( $_SESSION['brokerfield']['user_id'] ) ) {
    header( 'Location: index.php' );
}
if( isset( $_GET['logout'] ) && $_GET['logout'] == 1 ) {
    unset( $_SESSION['brokerfield'] );
}

if( !empty( $_POST['username'] ) ) {
    $username = isset( $_POST['username'] ) ? mysql_real_escape_string(trim($_POST['username'])) : '';
    $password = isset( $_POST['password'] ) ? mysql_real_escape_string(trim($_POST['password'])) : '';

    if( $username != '' && $password != '' ) {
        $sql = "SELECT * FROM users WHERE username = '{$username}' AND password = '{$password}' ";
        $result = mysql_query( $sql );
        if( mysql_num_rows( $result ) > 0 ) {
            $row = mysql_fetch_assoc( $result );
            $_SESSION['brokerfield']['user_id'] = $row['id'];
            $_SESSION['brokerfield']['username'] = $row['username'];
            header( 'Location: index.php' );
        }       
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Brookfield</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.js"> </script>
        <script type="text/javascript" src="js/jquery-ui.js"> </script>
        
        <script type="text/javascript" src="js/fancybox/source/jquery.fancybox.js?v=2.1.4" ></script>
        <link rel="stylesheet" type="text/css" href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
    </head>
    <body>
    <?php
    if( !empty( $_SESSION['brokerfield']['user_id'] ) ) {
        echo "<div class='top-link'>Welcome {$_SESSION['brokerfield']['username']} 
		| <a href='index.php'>Home</a> 
        | <a href='email.php'>Email</a> 
        | <a href='hit_list.php'>Hit</a> 
		| <a href='list.php'>Report</a>         
		| <a href='index.php?logout=1'>logout</a></div>";
    }
    ?>