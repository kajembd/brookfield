<?php include 'include/header.php'; ?>
<?php
    $block = isset( $_POST['block'] ) ? $_POST['block'] : '';
    $name = isset( $_POST['name'] ) ? $_POST['name'] : '';
    $email = isset( $_POST['email'] ) ? $_POST['email'] : '';
    $major = isset( $_POST['major'] ) ? $_POST['major'] : '';
    $age = isset( $_POST['age'] ) ? $_POST['age'] : '';
    $graduating_year = isset( $_POST['graduating_year'] ) ? $_POST['graduating_year'] : '';
    $gender = isset( $_POST['gender'] ) ? $_POST['gender'] : '';

    $flexible_working_hour = 0;
    $benifits = 0;
    $vacation = 0;
    $type_of_industry = 0;
    $company_reputation = 0;
    $company_culture = 0;
    $base_salary = 0;
    $location = 0;
    $job_responsibility = 0;
    $employer_choice= 0;
    
    if(!empty($_POST['flexible_working_hour'])){
        if($_POST['flexible_working_hour'] == 1) $flexible_working_hour = 1;
    }

    if(!empty($_POST['benifits'])){
        if($_POST['benifits'] == 1) $benifits = 1;
    }

    if(!empty($_POST['vacation'])){
        if($_POST['vacation'] == 1) $vacation= 1;
    }

    if(!empty($_POST['type_of_industry'])){
        if($_POST['type_of_industry'] == 1) $type_of_industry= 1;
    }
    
    if(!empty($_POST['company_reputation'])){
        if($_POST['company_reputation'] == 1) $company_reputation= 1;
    }

    if(!empty($_POST['company_culture'])){
        if($_POST['company_culture'] == 1) $company_culture= 1;
    }

    if(!empty($_POST['base_salary'])){
        if($_POST['base_salary'] == 1) $base_salary= 1;
    }

    if(!empty($_POST['location'])){
        if($_POST['location'] == 1) $location= 1;
    }

    if(!empty($_POST['job_responsibility'])){
        if($_POST['job_responsibility'] == 1) $job_responsibility= 1;
    }

    if(!empty($_POST['employer_choice'])){
        if($_POST['employer_choice'] == 1) $employer_choice= 1;
    }

    $other = isset( $_POST['other'] ) ? $_POST['other'] : '';
    
    $survey = array(
            'block' => $block,
            'name' => $name,
            'email' => $email,
            'major' => $major,
            'age' => $age,
            'graduating_year' => $graduating_year,            
            'gender' => $gender,
            'flexible_working_hour' => $flexible_working_hour,
            'vacation' => $vacation,
            'benifits' => $benifits,
            'type_of_industry' => $type_of_industry,
            'company_reputation' => $company_reputation,
            'company_culture' => $company_culture,
            'base_salary' => $base_salary,
            'location' => $location,
            'job_responsibility' => $job_responsibility,
            'employer_choice' => $employer_choice,
            'other' => $other,  
            'user_id' => $_SESSION['brokerfield']['user_id']
          );
?>
<div  id="temrs-conditions" > 
    <div class="term-conditions-content">
        <div class="term-conditions-header"></div>
        <form id="servey-form" method="post" action="confirm.php">
            <input type="hidden" name="block" value="<?php echo $block; ?>" />
            <input type="hidden" name="name" value="<?php echo $name; ?>" />
            <input type="hidden" name="email" value="<?php echo $email; ?>" />
            <input type="hidden" name="major" value="<?php echo $major; ?>" />
            <input type="hidden" name="age" value="<?php echo $age; ?>" />
            <input type="hidden" name="graduating_year" value="<?php echo $graduating_year; ?>" />
            <input type="hidden" name="gender" value="<?php echo $gender; ?>" />
            <input type="hidden" name="flexible_working_hour" value="<?php echo $flexible_working_hour; ?>" />
            <input type="hidden" name="benifits" value="<?php echo $benifits; ?>" />
            <input type="hidden" name="vacation" value="<?php echo $vacation; ?>" />
            <input type="hidden" name="type_of_industry" value="<?php echo $type_of_industry; ?>" />
            <input type="hidden" name="company_reputation" value="<?php echo $company_reputation; ?>" />
            <input type="hidden" name="company_culture" value="<?php echo $company_culture; ?>" />
            <input type="hidden" name="base_salary" value="<?php echo $base_salary; ?>" />
            <input type="hidden" name="location" value="<?php echo $location; ?>" />
            <input type="hidden" name="job_responsibility" value="<?php echo $job_responsibility; ?>" />
            <input type="hidden" name="employer_choice" value="<?php echo $employer_choice; ?>" />
            <input type="hidden" name="other" value="<?php echo $other; ?>" />
            <div class="term-conditions-text">            
               <div style="overflow: auto; height: 850px; padding-right: 10px;">				
                <p align="center"><b>BROOKFIELD RESIDENTIAL PROPERTIES INC. (the "Company")
                    Liability Waiver for Participants </b></p>
                    <p align="center"><b>ACKNOWLEDGEMENT AND RELEASE OF LIABILITY</b></p>
                    In exchange for participating in the Company's Mount Royal University career fair exhibition (the "<b>Exhibition</b>") held on or about March 13, 2013 on the property of Mount Royal University in Calgary, Alberta and on any other property or properties on which the Exhibition is located, which Exhibition includes, among other things, a tower approximately six feet tall made of modular blocks consisting of materials which may include wood, metal, plastics and other durable or dense materials, I <span style="font-weight: bold; color: gray;"><?php echo $name; ?></span> of the City of Calgary in the Province of Alberta, being at least 18 years of age, acknowledge that my use of and participation in the Exhibition is expressly conditioned on my agreement to each of the terms of this document.  I acknowledge and agree as follows:z
                    <ol>
                        <li>Use of the Exhibition involves activity that may cause injury to my person or damage to my property.  I acknowledge that there is an inherent risk, danger and hazard when choosing to participate in the Exhibition, which may be caused by, among other things, falling materials, the actions of other participants, faulty or defective components of the Exhibition, and other foreseen or unforeseen causes and hazards.  The risks to me include physical injury, death, illness, loss, and other damages, including loss of or damage to property.  My use of the Exhibition is voluntary in all respects and I assume the full risk of any injuries (including death), damages or loss which I may sustain as a result of participating in any and all activities arising out of, connected with, or in any way associated with my use of or participation in the Exhibition.</li>
                        <li>I do hereby fully release and discharge the Company, its affiliated companies and subsidiaries, and their respective employees, directors, officers, agents, successors and assigns (collectively, the "<b>Released Parties</b>") from any and all direct or indirect liability, claims, and causes of action for injuries or illness (including death), damages or loss to my person and/or my property (including but not limited to, legal fees on a solicitor and his own client basis) which I may have or which may accrue to me arising out of, connected with, or in any way associated with my use of or participation in the Exhibition.  This is a complete and irrevocable release and waiver of liability.  Specifically, and without limitation, I hereby release the Released Parties from any liability, claim, or cause of action arising out of the Released Parties' negligence, breach of contract, or breach of any statutory or other duty of care on the part of the Released Parties including the failure on the part of the Released Parties to safeguard or protect me from the risks, dangers and hazards of use of or participation in the Exhibition.</li>
                        <li>I further agree to indemnify and hold harmless and defend the Released Parties from any and all claims resulting from injuries or illness (including death), damages, or loss to person or property, including, but not limited to legal fees on a solicitor and his own client basis, sustained by me arising out of, connected with, or in any way associated with my use of or participation in the Exhibition.</li>
                        <li>In the event of any emergency, I authorize the Released Parties to secure from any hospital, physician and/or medical personnel any treatment deemed necessary for my immediate care and agree that I will be responsible for payment of any and all medical services rendered.</li>
                        <li>I certify that I am in good health and sufficient physical condition to properly use or participate in the Exhibition.</li>
                        <li>I understand and agree to adhere to all of the Company's<b> </b>Exhibition policy and rules.</li>
                        <li>This Acknowledgement and Release of Liability and any rights, duties and obligations as between the parties to this Acknowledgement and Release of Liability shall be governed by and interpreted solely in accordance with the laws of the Province of Alberta and no other jurisdiction.  I attorn to the exclusive jurisdiction of the courts in the Province of Alberta regarding any matter arising from or in connection with the Exhibition.</li>
                        <li>In entering into this Acknowledgement and Release of Liability I am not relying on any oral or written representations or statements made by the Released Parties including, without limited the generality of the foregoing, with respect to the safety of the Exhibition.</li>
                    </ol>
                    <b>I have read and fully understand the Acknowledgement and Release of Liability set forth above.  i acknowledge and i am aware that by signing this acknowledgement and release of liability i am waiving certain legal rights which i or my heirs, next of kin, executors, administrators, assigns and PERSONAL representatives may have against the released parties.  I ACKNOWLEDGE the permission to secure medical treatment and release of all claims, including claims for the negligence, breach of contract and breach of any statutory duty of care on the part of the Released Parties.  This document is binding upon me and my heirs, NEXT OF KIN, EXECUTORS, ADMINISTRATORS, ASSIGNS, personal representatives and anyone else entitled to act on my behalf. </b>
               </div>
               <div class="terms-submit">
                 <table>
                    <tr>
                        <td> <div id="signature-label" style="margin-top: 5px; float: left; margin-bottom: -5px; margin-right: 10px; font-size: 16px;"> Signature: </div> <div style=" float: left;  margin-bottom: -5px;"> <input type="text" name="signature" id="signature" value="<?php echo isset($_POST['signature']) ? $_POST['signature'] : '' ?>" /> </div> </td>
                    </tr>
                    <tr>
                        <td> 
                            <div class="radio-item">
                                <label  style="margin-top: 7px;" class="tCheckbox checkbox-btn-small checkbox-off-small" data-hidden-id="temrs-conditions-ckb"></label>
                                <label  class="checkbox-btn-small-label bigFont" id="i-agree">I agree to the attached acknowledgment and <br /> release of liability</label>
                            </div>
                            <input type="hidden" id="temrs-conditions-ckb" name="temrs-conditions" id="temrs-conditions" value='0' />
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            <div class="radio-item">
                                <label class="tCheckbox checkbox-btn-small checkbox-off-small" data-hidden-id="email_me"></label>
                                <label class="checkbox-btn-small-label bigFont" id="i-agree">Email me about career opportunities with Brookfield</label>
                            </div>
                            <input type="hidden" name="email_me" id="email_me" value='0' />
                    </tr>
                </table>

            </div>
           </div>
            </form>
            <div class="servey-button">
                <div class="back-servey"><button onclick="backToSurvey();" style="width: 150px; font-weight: bold; border: 1px solid #cdcdce; font-weight: bold;" class="btn btn-large btn-primary" onclick="proceedToSurvey();" > Back </button></div>
                <div class="submit-servey2"><button onclick="submitServey();" style="width: 150px; font-weight: bold; border: 1px solid #cdcdce; font-weight: bold;" class="btn btn-large btn-primary" onclick="proceedToSurvey();" > Submit </button></div>
                <div class="clear"></div>
            </div> 
        
            
        <div id="submit-confirm-popup" style="display: none;">
            <div class="submit-confirm-popup">
                <div class="thank-you">Thanks for playing! </div>
                <div class="return">Please return this device to a Brookfield team member. Good luck!</div>
                <div class="cancel" onclick="cancel();" >Cancel</div>
                <div class="continue" onclick="proceed();">Continue</div>
            </div>
        </div>
   </div>
</div>
<script>
    function submitServey(){
        error = '';
        
        var signature = $.trim($('#signature').val());
        
        if(!($('#temrs-conditions-ckb').val() == 1)){
            error += '<div> - You must agree to the terms and conditions . </div>';
            $('#i-agree').empty();
            $('#i-agree').html('*I agree to the attached acknowledgment and <br /> release of liability').addClass('error');
        }
        else{
            $('#i-agree').empty();
            $('#i-agree').html('I agree to the attached acknowledgment and <br /> release of liability').removeClass('error');
        }
        
        if(signature == ''){
            error += '<div> - Please enter the signature. </div>';
            $('#signature-label').empty();
            $('#signature-label').html('*Signature:').addClass('error');
        }
        else{
            $('#signature-label').empty();
            $('#signature-label').html('Signature:').removeClass('error');
        }
        
        if(error != ''){
            
        }else{
            $.fancybox({
                content: $('#submit-confirm-popup').html() 
            });
        }
    }
    
    function proceed(){
        var signature = $.trim($('#signature').val());
        var email_me = $.trim($('#email_me').val());
        var survey = '<?php echo http_build_query($survey); ?>';
        
        
         $.ajax({                    
             type: 'post',
             url: 'save_survey.php',
             data: survey + "&signature=" + signature + "&email_me=" + email_me,           
             success: function(){
                 $.fancybox({
                    content: $('#submit-confirm-popup').html() ,
                    closeBtn: true
                 });
                 window.location = 'confirm.php?block=<?php echo $block;  ?>';
             }
         });
    }
    
    function cancel(){
        $.fancybox.close();
    }
    
    function backToSurvey(){
        $('#servey-form').attr('action', 'survey.php');
        $('#servey-form').submit();
    }
    
    $(function(){        
       $(".tCheckbox").on("click", function(){
           var hiddenFieldId = $(this).attr("data-hidden-id"); 
           if($(this).hasClass("checkbox-off-small")){
             $(this).removeClass("checkbox-off-small").addClass("checkbox-on-small");   
             $("#"+hiddenFieldId).val(1);
           }else{
               $(this).removeClass("checkbox-on-small").addClass("checkbox-off-small");   
               $("#"+hiddenFieldId).val(0);
           }
       });
    });
    
</script>
<?php include 'include/footer.php'; ?>

