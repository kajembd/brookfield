<html>
<head>
<title>PHPMailer - SMTP basic test with authentication</title>
</head>
<body>

<?php

//error_reporting(E_ALL);
error_reporting(E_STRICT);
error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('America/Toronto');

require_once('phpmailer/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail             = new PHPMailer();

$body             = file_get_contents('contents.html');

//$body             = preg_replace('/[\]/','',$body);
$mail->IsSMTP(); // telling the class to use SMTP
$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->Host       = "mail.instaonline.com"; // sets the SMTP server
$mail->Port       = 25;                    // set the SMTP port for the GMAIL server
$mail->Username   = "development@instaonline.com"; // SMTP account username
$mail->Password   = "dev8545MENT";        // SMTP account password
$mail->SetFrom('development@instaonline.comm', 'First Last');
$mail->Subject    = "PHPMailer Test Subject via smtp, basic with authentication";
$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->MsgHTML($body);

$address = "sarwar@instalogic.com";            // TO ADDRESS

$mail->AddAddress($address, "John Doe");
$mail->AddAttachment("images/phpmailer.gif");      // attachment
$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

if(!$mail->Send()) {
  echo "Mailer Error: ";
  echo "<pre>"; 
  print_r(  $mail->ErrorInfo); 
  echo "</pre>"; 
} else {
  echo "Message sent!";
}

?>

</body>
</html>
