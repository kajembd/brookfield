<?php
    include 'include/header.php';
?>
<?php 
    $block = isset($_POST['block']) ? $_POST['block'] : '';
    $age = isset($_POST['age']) ? $_POST['age'] : '18-21'; 
    $gender = isset( $_POST['gender'] ) ? $_POST['gender'] : 'Male';    
    
    $flexible_working_hour = 0;
    $benifits = 0;
    $vacation= 0;
    $type_of_industry= 0;
    $company_reputation= 0;
    $company_culture= 0;
    $base_salary= 0;
    $location= 0;
    $job_responsibility= 0;
    $employer_choice= 0;
    
    if(!empty($_POST['flexible_working_hour'])){
        if($_POST['flexible_working_hour'] == 1) $flexible_working_hour = 1;
    }

    if(!empty($_POST['benifits'])){
        if($_POST['benifits'] == 1) $benifits = 1;
    }

    if(!empty($_POST['vacation'])){
        if($_POST['vacation'] == 1) $vacation= 1;
    }

    if(!empty($_POST['type_of_industry'])){
        if($_POST['type_of_industry'] == 1) $type_of_industry= 1;
    }
    
    if(!empty($_POST['company_reputation'])){
        if($_POST['company_reputation'] == 1) $company_reputation= 1;
    }

    if(!empty($_POST['company_culture'])){
        if($_POST['company_culture'] == 1) $company_culture= 1;
    }

    if(!empty($_POST['base_salary'])){
        if($_POST['base_salary'] == 1) $base_salary= 1;
    }

    if(!empty($_POST['location'])){
        if($_POST['location'] == 1) $location= 1;
    }

    if(!empty($_POST['job_responsibility'])){
        if($_POST['job_responsibility'] == 1) $job_responsibility= 1;
    }

    if(!empty($_POST['employer_choice'])){
        if($_POST['employer_choice'] == 1) $employer_choice= 1;
    }
    
?>
<div id="survey">
    <div class="servey-content">
      <form id="form-servey" action="terms_conditions.php" method="post">
            <?php 
            if(isset($_POST['signature'])){
                echo "<input type=\"hidden\" name=\"signature\" value=".$_POST['signature'].">";
            } 
            ?>
            <input type="hidden" name="block" value="<?php echo $block; ?>">
        <div class="contact-info">
            <h3>Tell Us About Yourself</h3>
            <table class="name-email form-tbl" cellspacing="0" cellpadding="0">
                <tr>
                    <th style="width: 70px;" id="name-label"> Name: </th>
                    <td> <input type="text" class="input-xlarge" name="name" id="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>" placeholder="" /> <span  ></span> </td>
                </tr>
                <tr>
                    <th id="email-label"> Email Address: </th>
                    <td> <input type="text" class="input-xlarge" name="email" id="email" placeholder="" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>" /> </td>
                </tr>
                <tr>
                    <th> Major: </th>
                    <td> <input type="text" class="input-xlarge" name="major" id="major" placeholder="" value="<?php echo isset($_POST['major']) ? $_POST['major'] : ''; ?>" /> </td>
                </tr>     
                <tr>
                    <th id="age-label" style="width:70px;"> Age: </th>
                    <td align="left">
						<table class=age-table>
							<tr>
								<td><label class="ageRadio radio-btn-small radio-on-small" data-value="18-21" id="age_18_21"></label></td>
								<td><label class="ageRadio radio-btn-small radio-off-small" data-value="22-25" id="age_22_25"></label></td>
								<td><label class="ageRadio radio-btn-small radio-off-small" data-value="26-30" id="age_26_30"></label></td>
								<td><label class="ageRadio radio-btn-small radio-off-small" data-value="31-40" id="age_31_40"></label></td>
								<td><label class="ageRadio radio-btn-small radio-off-small" data-value="41+" id="age_41_plus"></label></td>
							</tr>
							<tr>
								<td><label class="ageRadio-label"> 18-21 </label></td>
								<td><label class="ageRadio-label"> 22-25 </label></td>
								<td><label class="ageRadio-label"> 26-30 </label></td>
								<td><label class="ageRadio-label"> 31-40 </label></td>
								<td><label style="padding-left: 5px !important;" class="ageRadio-label"> 41+ </label></td>
							</tr>
						</table>
                        <!--div class="age-radio-item">
                            <label class="ageRadio radio-btn-small radio-on-small" data-value="18-21" id="age_18_21"></label>
                            <label class="ageRadio radio-btn-small radio-off-small" data-value="22-25" id="age_22_25"></label>
                            <label class="ageRadio radio-btn-small radio-off-small" data-value="26-30" id="age_26_30"></label>
                            <label class="ageRadio radio-btn-small radio-off-small" data-value="31-40" id="age_31_40"></label>
                            <label class="ageRadio radio-btn-small radio-off-small" data-value="41+" id="age_41_plus"></label>
                            <label class="clear"></label>
                        </div> 
                        <div class="age-radio-label">
                            <label class="ageRadio-label"> 18-21 </label>
                            <label class="ageRadio-label"> 22-25 </label>
                            <label class="ageRadio-label"> 26-30 </label>
                            <label class="ageRadio-label"> 31-40 </label>
                            <label style="padding-left: 52px;" class="ageRadio-label"> 41+ </label>
                        </div-->
                        <input type="hidden" name="age" id="age" value="18-21" />
                    </td>
                </tr>
            </table>
            <table class="form-tbl" cellspacing="0" cellpadding="0">
                <tr>
                    <th id="graduating-year-label" width="70px" > Graduating  Year: </th>
                    <td> <input class="input-graduating-year" type="text" name="graduating_year" id="graduating_year" placeholder="" value="<?php echo isset($_POST['graduating_year']) ? $_POST['graduating_year'] : ''; ?>" /> </td>

                    <td>
                        <div style="margin-top: -5px;" class="radio-item">
                            <label class="genderRadio radio-btn-small radio-on-small" data-value="Male" id="Male"></label>
                            <label class="radio-btn-small-label"> M </label>
                        </div>
                        <div style="margin-top: -5px; padding-left: 10px;" class="radio-item">
                            <label class="genderRadio radio-btn-small radio-off-small" data-value="Female" id="Female"></label>
                            <label class="radio-btn-small-label">F</label>
                        </div>
                        <input type="hidden" name="gender" id="gender" value="<?php echo $gender; ?>" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="survery-question">
            <h3 class="survery-question-label">When applying to a job, what are the  <br /> top 3 things that are most important to you?</h3>
            <table>
                <tr>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="flexible_working_hour"></label>
                            <label class="radio-btn-small-label"> Flexible Working Hours </label>
                        </div>
                        <input type="hidden" name="flexible_working_hour" id="flexible_working_hour" value='<?php echo $flexible_working_hour; ?>' />
                    </td>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="vacation"></label>
                            <label class="radio-btn-small-label"> Vacation </label>
                        </div>
                        <input type="hidden" name="vacation" id="vacation" value='<?php echo $vacation; ?>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="benifits"></label>
                            <label class="radio-btn-small-label"> Benefits </label>
                        </div>
                        <input type="hidden" name="benifits" id="benifits" value='<?php echo $benifits; ?>' />
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="type_of_industry"></label>
                            <label class="radio-btn-small-label"> Type of Industry </label>
                        </div>
                        <input type="hidden" name="type_of_industry" id="type_of_industry" value='<?php echo $type_of_industry; ?>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="company_reputation"></label>
                            <label class="radio-btn-small-label"> Company Reputation </label>
                        </div>
                        <input type="hidden" name="company_reputation" id="company_reputation" value='<?php echo $company_reputation; ?>' />
                    </td>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="company_culture"></label>
                            <label class="radio-btn-small-label"> Company Culture </label>
                        </div>
                        <input type="hidden" name="company_culture" id="company_culture" value='<?php echo $company_culture; ?>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="base_salary"></label>
                            <label class="radio-btn-small-label">Base Salary </label>
                        </div>
                        <input type="hidden" name="base_salary" id="base_salary" value='<?php echo $base_salary; ?>' />
                    </td>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="location"></label>
                            <label class="radio-btn-small-label">Location </label>
                        </div>
                        <input type="hidden" name="location" id="location" value='<?php echo $location; ?>' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="job_responsibility"></label>
                            <label class="radio-btn-small-label">Specific Job Responsibilities </label>
                        </div>
                        <input type="hidden" name="job_responsibility" id="job_responsibility" value='<?php echo $job_responsibility; ?>' />
                    </td>
                    <td>
                        <div class="radio-item">
                            <label class="gCheckbox radio-btn-small radio-off-small" data-hidden-id="employer_choice"></label>
                            <label class="radio-btn-small-label">Canada’s Top Employers of Choice </label>
                        </div>
                        <input type="hidden" name="employer_choice" id="employer_choice" value='<?php echo $employer_choice; ?>' />
                    </td>
                </tr>
            </table>
            
            <div class="other" style="padding-left:10px; "><input type="text" style="width:450px;" name="other" placeholder="Other" id="other" value="<?php echo isset($_POST['other']) ? $_POST['other'] : ''; ?>"  /></div>           
        </div>       
        </form>
        <div class="submit-servey"><button onclick="submitServey();" style="width: 150px; font-weight: bold; border: 1px solid #cdcdce; font-weight: bold;" class="btn btn-large btn-primary" onclick="proceedToSurvey();" > Next </button></div>               
      
    </div>
</div>

<?php
    include 'include/footer.php';
?>

<script type="text/javascript">
    function popup(){
        $('a.fancybox-item').css('display', 'block !important');
        $.fancybox({
            content: $('#temrs-conditions').html() 
        });
    }
    
    function submitServey(){
        
        var name = $.trim($('#name').val());
        var email = $.trim($('#email').val());
        var age = $.trim($('#age').val());
        var graduating_year = $.trim($('#graduating_year').val());
        var question = 0;
        
        if( $.trim($('#other').val()) != '') {
            question = 1;
        }
        
        $(".survery-question").find("input:hidden").each(function(){
           if($(this).val() == 1) question++;
        });
        
        var error = '';        

        if(name == ''){
            error += '<div> - Please enter name. </div>';
            $('#name-label').empty();
            $('#name-label').html('*Name').addClass('error');
            $('#name').css('border', 'f0b310');            
        }
        else{
            $('#name-label').empty();
            $('#name-label').html('Name').removeClass('error');
        }
        if(email == ''){
            error += '<div> - Please enter email. </div>';
            $('#email-label').empty();
            $('#email-label').html('*Email').addClass('error');
        }
        else{
            if(!isValidEmailAddress(email)){
                error += '<div> - Please enter a valid email. </div>';
                $('#email-label').empty();
                $('#email-label').html('*Email').addClass('error');
            }
            else{
                $('#email-label').empty();
                $('#email-label').html('Email').removeClass('error');
            }
        }
        
        if(graduating_year == ''){
            error += '<div> - Please enter Graduating Year. </div>';
            $('#graduating-year-label').empty();
            $('#graduating-year-label').html('*Graduating Year').addClass('error');
        }
        else{
            if(!$.isNumeric(graduating_year) || graduating_year < 1 ){
                error += '<div> - Please enter a valid Graduating Year. </div>';
                $('#graduating-year-label').empty();
                $('#graduating-year-label').html('*Graduating Year').addClass('error');
            }
            else{
                $('#graduating-year-label').empty();
                $('#graduating-year-label').html('Graduating Year').removeClass('error');
            }
        }
        
        if(question < 3){            
            $('.survery-question-label').empty();
            $('.survery-question-label').html('*When applying to a job, what are the  <br /> top 3 things that are most important to you?').addClass('error');
            error += "<div> - Please select at least three optiosn form question: 'When applying to a job, what are the top 3 things that are most important to you?'. </div>";
        }
        else{
            $('.survery-question-label').empty();
            $('.survery-question-label').html('When applying to a job, what are the  <br /> top 3 things that are most important to you?').removeClass('error');
        }
        
        if(error != ''){
            //$.fancybox({
            //content: '<div style="background-color: #FFFFFF; padding: 15px; color: red; font-size: 12px; border-radius: 7px"><div style="padding-bottom: 3px; font-size: 16px;"> Error occured! Please see below.</div>'+ error +'</div>' 
       // });
        }else{
            $('#form-servey').submit();
        }
    }
    
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        //alert( pattern.test(emailAddress) );
        return pattern.test(emailAddress);
    };    
    
    $(function(){
       
       var age = '<?php echo $age; ?>';
       
       $("#age_18_21").removeClass("radio-on-small").addClass("radio-off-small"); 
       
       if( age != '' ){
           if( age == '18-21' ){
               $("#age_18_21").removeClass("radio-off-small").addClass("radio-on-small"); 
           }else if( age == '22-25'  ){
               $("#age_22_25").removeClass("radio-off-small").addClass("radio-on-small"); 
           }else if( age == '26-30'  ){
               $("#age_26_30").removeClass("radio-off-small").addClass("radio-on-small"); 
           }else if( age == '31-40'  ){
               $("#age_31_40").removeClass("radio-off-small").addClass("radio-on-small"); 
           }else if( age == '41+'  ){
               $("#age_41_plus").removeClass("radio-off-small").addClass("radio-on-small"); 
           }           
           $("#age").val(age); 
       }
       
       var gender = '<?php echo $gender; ?>';
       
       if(gender == 'Male'){
           $("#Male").removeClass("radio-off-small").addClass("radio-on-small"); 
           $("#Female").removeClass("radio-on-small").addClass("radio-off-small"); 
       }else{
           $("#Male").removeClass("radio-on-small").addClass("radio-off-small"); 
           $("#Female").removeClass("radio-off-small").addClass("radio-on-small");
       }
       
       var flexible_working_hour = '<?php echo $flexible_working_hour; ?>';
       var benifits = '<?php echo $benifits; ?>';
       var vacation = '<?php echo $vacation; ?>';
       var type_of_industry = '<?php echo $type_of_industry; ?>';
       var company_reputation = '<?php echo $company_reputation; ?>';
       var company_culture = '<?php echo $company_culture; ?>';
       var base_salary = '<?php echo $base_salary; ?>';
       var location = '<?php echo $location; ?>';
       var job_responsibility = '<?php echo $job_responsibility; ?>';
       var employer_choice = '<?php echo $employer_choice; ?>';
       
       if(flexible_working_hour == '1'){
           $('label[data-hidden-id="flexible_working_hour"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(benifits == '1'){
           $('label[data-hidden-id="benifits"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(vacation == '1'){
           $('label[data-hidden-id="vacation"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(type_of_industry == '1'){
           $('label[data-hidden-id="type_of_industry"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(company_reputation == '1'){
           $('label[data-hidden-id="company_reputation"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(company_culture == '1'){
           $('label[data-hidden-id="company_culture"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(base_salary == '1'){
           $('label[data-hidden-id="base_salary"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(location == '1'){
           $('label[data-hidden-id="location"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(job_responsibility == '1'){
           $('label[data-hidden-id="job_responsibility"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       if(employer_choice == '1'){
           $('label[data-hidden-id="employer_choice"]').removeClass("radio-off-small").addClass("radio-on-small");   
       }
       
       $(".ageRadio").on("click", function(){
             var value = $(this).attr("data-value"); 
      
             if(value == "18-21"){
                 $("#age_18_21").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#age_22_25").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_26_30").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_31_40").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_41_plus").removeClass("radio-on-small").addClass("radio-off-small"); 
             }else if(value == "22-25"){                 
                 $("#age_18_21").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_22_25").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#age_26_30").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_31_40").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_41_plus").removeClass("radio-on-small").addClass("radio-off-small"); 
             }else if(value == "26-30"){
                 $("#age_18_21").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_22_25").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_26_30").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#age_31_40").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_41_plus").removeClass("radio-on-small").addClass("radio-off-small"); 
             }else if(value == "31-40"){
                 $("#age_18_21").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_22_25").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_26_30").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_31_40").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#age_41_plus").removeClass("radio-on-small").addClass("radio-off-small"); 
             }else if(value == "41+"){
                 $("#age_18_21").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_22_25").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_26_30").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_31_40").removeClass("radio-on-small").addClass("radio-off-small"); 
                 $("#age_41_plus").removeClass("radio-off-small").addClass("radio-on-small"); 
             }
             $("#age").val(value); 
       }) ;
        
       $(".genderRadio").on("click", function(){
             var value = $(this).attr("data-value"); 
             
             if(value == "Male"){
                 $("#Male").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#Female").removeClass("radio-on-small").addClass("radio-off-small"); 
             }else{
                 $("#Female").removeClass("radio-off-small").addClass("radio-on-small"); 
                 $("#Male").removeClass("radio-on-small").addClass("radio-off-small"); 
             }
             $("#gender").val(value); 
       }) ;
       
       $(".gCheckbox").on("click", function(){
           var hiddenFieldId = $(this).attr("data-hidden-id"); 
		   
		    var question = 0;
		    if( $.trim($('#other').val()) != '') {
            question = 1;
			}
			
			$(".survery-question").find("input:hidden").each(function(){
			   if($(this).val() == 1) question++;
			});
		   
           if($(this).hasClass("radio-on-small")){
             $(this).removeClass("radio-on-small").addClass("radio-off-small");   
             $("#"+hiddenFieldId).val(0);
			 $('#other').attr('readonly', false);
           }else if(question < 3){
               $(this).removeClass("radio-off-small").addClass("radio-on-small");   
               $("#"+hiddenFieldId).val(1);
			   
			   if($.trim($('#other').val()) == '' && question == '2'){
					$('#other').attr('readonly', true);
			   }else{
					$('#other').attr('readonly', false);
			   }
           }
       });
       
       $(".tCheckbox").on("click", function(){
           var hiddenFieldId = $(this).attr("data-hidden-id"); 
           if($(this).hasClass("checkbox-off-small")){
             $(this).removeClass("checkbox-off-small").addClass("checkbox-on-small");   
             $("#"+hiddenFieldId).val(1);
           }else{
               $(this).removeClass("checkbox-on-small").addClass("checkbox-off-small");   
               $("#"+hiddenFieldId).val(0);
           }
       });
       
    });
</script>